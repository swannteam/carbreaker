package com.swannteam.carbreaker.game;

import java.io.IOException;

import javax.vecmath.Vector3f;

import android.content.Context;
import android.util.Log;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.linearmath.Transform;
import com.threed.jpct.Loader;
import com.threed.jpct.Object3D;

public class DefaultGameConfig implements IGameConfig {

	// protected final String groundTexturePath = "";
	// protected String[] textures;

	private final String groundShapePath = "ground.3ds";
	private final String groundGraphicPath = "ground.3ds";
	
	private final String carShapePath = "simplecar.3ds";
	private final String carGraphicPath = "car.3ds";

	protected final Transform[] startTransforms;
	protected final Transform[] centersOfMass;

	protected final byte maxPlayers = 3;
	protected final int carMass = 1700;
	protected final int carHealth = 1000;

	protected final Vector3f[] connectionPoints = new Vector3f[] {
			new Vector3f(-0.9f, -0.2f, 1.6f), new Vector3f(0.9f, -0.2f, 1.6f),
			new Vector3f(-0.9f, -0.2f, -1.3f), new Vector3f(0.9f, -0.2f, -1.3f) };

	protected final float suspensionRestLength = 0.6f;
	protected final float wheelRadius = 0.45f;

	protected CollisionShape groundCollisionShape;
	protected CollisionShape mountCollisionShape;
	
	@Override
	public CollisionShape getMountCollisionShape() {
		return mountCollisionShape;
	}


	protected Object3D groundGraphics;
	protected Object3D mountGraphics;
	
	@Override
	public Object3D getMountGraphics() {
		return mountGraphics;
	}


	protected String groundTexture;
	protected String mountTexture;

	protected CollisionShape carCollisionShape;
	protected Object3D carGraphics;

	@Override
	public byte getMaxPlayers() {
		return maxPlayers;
	}

	public int getCarMass() {
		return carMass;
	}

	public float getCarHealth() {
		return carHealth;
	}

	@Override
	public Object3D getGroundGraphics() {
		return groundGraphics;
	}

	@Override
	public CollisionShape getCarCollisionShape() {
		return carCollisionShape;
	}

	@Override
	public Object3D getCarGraphics() {
		return carGraphics;
	}

	@Override
	public Transform[] getStartTransforms() {
		return startTransforms;
	}

	@Override
	public CollisionShape getGroundCollisionShape() {
		return groundCollisionShape;
	}

	@Override
	public Vector3f[] getConnectionPoints() {
		return connectionPoints;
	}

	@Override
	public float getSuspensionRestLength() {
		return suspensionRestLength;
	}

	@Override
	public float getWheelRadius() {
		return wheelRadius;
	}

	public DefaultGameConfig() {
		startTransforms = new Transform[maxPlayers];
		centersOfMass = new Transform[maxPlayers];
	}

	public boolean initialize(Context context) {
		startTransforms[0] = new Transform();
		startTransforms[1] = new Transform();
		startTransforms[2] = new Transform();

		startTransforms[0].setIdentity();
		startTransforms[1].setIdentity();
		startTransforms[2].setIdentity();

		startTransforms[0].origin.set(new Vector3f(65, 10, 0));
		startTransforms[1].origin.set(new Vector3f(60, 10, 0));
		startTransforms[2].origin.set(new Vector3f(55, 10, 70));
		
		centersOfMass[0] = new Transform();
		centersOfMass[1] = new Transform();
		centersOfMass[2] = new Transform();

		centersOfMass[0].setIdentity();
		centersOfMass[1].setIdentity();
		centersOfMass[2].setIdentity();

		centersOfMass[0].origin.set(new Vector3f(65, 5, 0));
		centersOfMass[1].origin.set(new Vector3f(60, 5, 0));
		centersOfMass[2].origin.set(new Vector3f(55, 5, 70));
		

		//synchronized (context) {

			try {
				groundCollisionShape = GameUtils.CreateConvaceFrom3ds(context
						.getAssets().open("fisground.3ds"), 5);
				
				groundGraphics = Loader.load3DS(context
						.getAssets().open(groundGraphicPath), 5)[0];

				mountGraphics = Loader.load3DS(context
						.getAssets().open(groundGraphicPath), 5)[1];
				
				carGraphics = Object3D.mergeAll(Loader.load3DS(
						context.getAssets().open(carGraphicPath), 1));
				
				//Log.i(Game.TAG, "Game config: car loaded");
				
				carCollisionShape = GameUtils.CreateConvexFromObject(Object3D
						.mergeAll(Loader.load3DS(
								context.getAssets().open(carShapePath), 1)));
			} catch (IOException e) {
				return false;
			}
		//}
		return true;
	}

	@Override
	public Transform[] getCentersOfMass() {
		
		return centersOfMass;
	}
}

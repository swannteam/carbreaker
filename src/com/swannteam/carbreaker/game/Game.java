package com.swannteam.carbreaker.game;

import java.io.IOException;
import java.util.ArrayList;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import com.swannteam.carbreaker.MainActivity;
import com.swannteam.carbreaker.network.Client;
import com.swannteam.carbreaker.network.Server;
import com.swannteam.carbreaker.world.RealBody;
import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Logger;
import com.threed.jpct.Object3D;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.SkyBox;

public class Game {

	public static final String TAG = "CarBreaker";

	private static Game instance = null;

	private MainActivity activity = null;
	private GLSurfaceView glView = null;
	private FrameBuffer frameBuffer = null;
	private Renderer renderer = null;
	private Camera camera = null;

	private Client client = null;
	private Server server = null;

	private Game() {
	}

	public static Game getInstance() {
		if (instance == null)
			instance = new Game();
		return instance;
	}

	public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public MainActivity getActivity() {
		return activity;
	}

	public void setActivity(MainActivity activity) {
		this.activity = activity;
	}

	public GLSurfaceView getGlView() {
		return glView;
	}

	public void setGlView(GLSurfaceView glView) {
		this.glView = glView;
	}

	public FrameBuffer getFrameBuffer() {
		return frameBuffer;
	}

	public void setFrameBuffer(FrameBuffer frameBuffer) {
		this.frameBuffer = frameBuffer;
	}

	public Renderer getRenderer() {
		return renderer;
	}

	public void setRenderer(Renderer renderer) {
		this.renderer = renderer;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public void loadTextures() {
		try {

			Texture front = new Texture(Game.getInstance().getActivity()
					.getAssets().open("front.jpg"));

			Texture bottom = new Texture(Game.getInstance().getActivity()
					.getAssets().open("bottom.jpg"));

			Texture back = new Texture(Game.getInstance().getActivity()
					.getAssets().open("back.jpg"));

			Texture left = new Texture(Game.getInstance().getActivity()
					.getAssets().open("left.jpg"));

			Texture right = new Texture(Game.getInstance().getActivity()
					.getAssets().open("right.jpg"));

			Texture top = new Texture(Game.getInstance().getActivity()
					.getAssets().open("top.jpg"));

			Texture grass = new Texture(Game.getInstance().getActivity()
					.getAssets().open("grass.jpg"));

			Texture stone = new Texture(Game.getInstance().getActivity()
					.getAssets().open("stone.jpg"));

			TextureManager.getInstance().addTexture("up", top);
			TextureManager.getInstance().addTexture("right", right);
			TextureManager.getInstance().addTexture("left", left);
			TextureManager.getInstance().addTexture("front", front);
			TextureManager.getInstance().addTexture("down", bottom);
			TextureManager.getInstance().addTexture("back", back);

			// TextureManager.getInstance().removeTexture("grass.jpg");
			TextureManager.getInstance().addTexture("stone.jpg", grass);
			TextureManager.getInstance().addTexture("grass.jpg", stone);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void unloadTextures() {
		TextureManager.getInstance().removeTexture("up");
		TextureManager.getInstance().removeTexture("right");
		TextureManager.getInstance().removeTexture("left");
		TextureManager.getInstance().removeTexture("front");
		TextureManager.getInstance().removeTexture("down");
		TextureManager.getInstance().removeTexture("back");

		// TextureManager.getInstance().removeTexture("grass.jpg");
		TextureManager.getInstance().removeTexture("stone.jpg");
		TextureManager.getInstance().removeTexture("grass.jpg");
	}
	
	public void init(FrameBuffer frameBuffer) {
		Logger.log("Game init");
		this.frameBuffer = frameBuffer;

	}

	private float forceinc = 600;
	private float maxforce = 6000;
	private float currforce = 0;
	private float breakforce = 0;
	private float steeringinc = (float) Math.PI / 180;
	private float currsteering = 0;

	public void recalc() {

		if (client == null || !client.getGameThread().isAlive()) {
			return;
		}

		controlUpdate();

		if (counter % 4 == 0) {
			counter -= 4;
			client.getGameThread().setCarData(currsteering, currforce,
					breakforce);
		}

		activity.setHealth((Integer.toString((int) client.getGameThread()
				.getHealth())));
		client.draw(frameBuffer);

		++counter;
	}

	long counter = 0;

	private void controlUpdate() {
		if (activity.getJoysticForce().getPosition()[1] < 0) {
			breakforce = 0;
			if (currforce < maxforce) {
				currforce += forceinc;
			}
		} else if (activity.getJoysticForce().getPosition()[1] > 0) {
			if (currforce > -1000) {
				currforce -= forceinc;
			}
			// breakforce = 100;
		} else {
			currforce = 0;
		}

		if ((activity.getJoystic().getPosition()[0] < 0)
				&& (currsteering < (float) Math.PI / 4
						* Math.abs(activity.getJoystic().getPosition()[0])
						/ 175)) {
			if ((Math.abs(activity.getJoystic().getPosition()[0])) < (Math
					.abs(activity.getJoystic().getLayoutWidth() / 2))) {
				currsteering += steeringinc;
			}
		} else if ((activity.getJoystic().getPosition()[0] > 0)
				&& (currsteering > -(float) Math.PI / 4
						* Math.abs(activity.getJoystic().getPosition()[0])
						/ 175)) {
			if ((Math.abs(activity.getJoystic().getPosition()[0])) < (Math
					.abs(activity.getJoystic().getLayoutWidth() / 2))) {
				currsteering -= steeringinc;
			}
		} else {
			if ((currsteering > 0)) {
				currsteering -= steeringinc;
				if (currsteering < 0)
					currsteering = 0;
			} else if ((currsteering < 0)) {
				currsteering += steeringinc;
				if (currsteering > 0)
					currsteering = 0;
			}
		}

	}
	/****************************************************************************************************************************/
}

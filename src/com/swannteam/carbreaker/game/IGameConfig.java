package com.swannteam.carbreaker.game;

import javax.vecmath.Vector3f;

import android.content.Context;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.linearmath.Transform;
import com.threed.jpct.Object3D;

public interface IGameConfig {
	
	boolean initialize(Context context);
	
	CollisionShape getCarCollisionShape();
	Object3D getCarGraphics();
	
	CollisionShape getGroundCollisionShape();
	Object3D getGroundGraphics();
	Object3D getMountGraphics();
	//String getGroundTexture();
	
	int getCarMass();
	float getCarHealth();
	
	Transform[] getStartTransforms();
	Transform[] getCentersOfMass();
	Vector3f[] getConnectionPoints();
	float getSuspensionRestLength();
	float getWheelRadius();

	CollisionShape getMountCollisionShape();

	byte getMaxPlayers();
}

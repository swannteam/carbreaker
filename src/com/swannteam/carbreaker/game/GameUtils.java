package com.swannteam.carbreaker.game;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BvhTriangleMeshShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.collision.shapes.ConvexShape;
import com.bulletphysics.collision.shapes.TriangleIndexVertexArray;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;
import com.swannteam.carbreaker.utils.parser3ds.Model;
import com.swannteam.carbreaker.utils.parser3ds.ModelLoader;
import com.swannteam.carbreaker.utils.parser3ds.ModelObject;
import com.swannteam.carbreaker.utils.parser3ds.ParserException;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;

public class GameUtils {

	public static RigidBody CreateRigidBodyFrom3D(Object3D object, float mass, Vector3f localInertia, Transform startTransform){
		return CreateRigidBodyFromShape(CreateConvexFromObject(object), mass, localInertia, startTransform);
	}
	
	public static BvhTriangleMeshShape CreateConvaceFrom3ds(InputStream stream, float scale) {
		
		Model model = null;
		
		try {
			model = ModelLoader.load3dModel(stream);
		} catch (ParserException e) {
			e.printStackTrace();
		}
	
		ModelObject obj = model.objects.getFirst();
		
		int totalVerts = obj.vertices.length / 3;
		int totalTriangles = obj.polygons.length / 3;
		
		int vertStride = 3 * 4;
		int indexStride = 3 * 4;
		
		ByteBuffer gVertices = ByteBuffer.allocateDirect(totalVerts * 3 * 4).order(ByteOrder.nativeOrder());
		ByteBuffer gIndices = ByteBuffer.allocateDirect(totalTriangles * 3 * 4).order(ByteOrder.nativeOrder());		
		
		for(int i = 0; i < obj.vertices.length; i += 3) {
			gVertices.putFloat(i * 4 + 0, obj.vertices[i] * scale);
			gVertices.putFloat(i * 4 + 4, -obj.vertices[i+1] * scale);
			gVertices.putFloat(i * 4 + 8, -obj.vertices[i+2] * scale);
		}
		  
		gIndices.clear();
		
		for(int i = 0; i < obj.polygons.length; i += 3) {
			gIndices.putInt(i * 4 + 0, obj.polygons[i]);
			gIndices.putInt(i * 4 + 4, obj.polygons[i+1]);
			gIndices.putInt(i * 4 + 8, obj.polygons[i+2]);
		}		
		
		TriangleIndexVertexArray indexVertexArrays = new TriangleIndexVertexArray(totalTriangles,
			gIndices,
			indexStride,
			totalVerts, gVertices, vertStride);
		
		boolean useQuantizedAabbCompression = true;
		
		return new BvhTriangleMeshShape(indexVertexArrays, useQuantizedAabbCompression);		
	}
	
	public static ConvexShape CreateConvexFromObject(Object3D object) {

		VertexControl objControl = new VertexControl();
		objControl.init(object.getMesh(), true);
		
		SimpleVector[]  objVectors = objControl.getSourceMesh();
		ObjectArrayList<Vector3f> objVertexes = new ObjectArrayList<Vector3f>();
		
		for(SimpleVector vector : objVectors) {
			objVertexes.add(new Vector3f(vector.x , -vector.y, -vector.z ));
		}
		
		return new ConvexHullShape(objVertexes);		
	}
	
	
	public static RigidBody CreateRigidBodyFromShape(CollisionShape shape, float mass, Vector3f localInertia, Transform startTransform) {
	
		if (mass != 0f) {
			shape.calculateLocalInertia(mass, localInertia);
		}

		DefaultMotionState myMotionState = new DefaultMotionState(startTransform);
		
		RigidBodyConstructionInfo cInfo = new RigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
	
		return new RigidBody(cInfo);
	}

}

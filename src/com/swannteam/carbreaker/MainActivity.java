package com.swannteam.carbreaker;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.swannteam.carbreaker.game.Game;
import com.swannteam.carbreaker.graphics.GameGLSurfaceView;
import com.swannteam.carbreaker.graphics.GameRenderer;
import com.swannteam.carbreaker.graphics.joystick.JoyStickClass;
import com.swannteam.carbreaker.network.events.ClientAdapter;
import com.swannteam.carbreaker.network.events.ClientEvent;
import com.swannteam.carbreaker.network.events.ServerAdapter;
import com.swannteam.carbreaker.network.events.ServerEvent;

/**
 * Main activity
 * 
 * @author Pumpurum
 */
public class MainActivity extends Activity {

	public static final int UPDATE_HEALT = 0;
	public static final int INITIALIZED = 1;
	public static final int READY = 2;

	private static final int JOYSTICK_ALPHA = 150;
	private static final int STICK_ALPHA = 100;
	private static final int STICK_SIZE = 100;
	
	private Handler handler;

	private RelativeLayout layout_joystick;
	private RelativeLayout layout_joystick_force;

	private JoyStickClass joystic;
	private JoyStickClass joysticforce;
	private TextView healthText;
	private Button buttonReady;
	private Button buttonR;
	private ViewFlipper flipper;

	public JoyStickClass getJoystic() {
		return joystic;
	}

	public void setJoystic(JoyStickClass joystic) {
		this.joystic = joystic;
	}

	public void setJoysticForce(JoyStickClass joysticforce) {
		this.joysticforce = joysticforce;
	}

	public JoyStickClass getJoysticForce() {
		return joysticforce;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.gamescreen);
		flipper = (ViewFlipper) findViewById(R.id.gameflipper);
		flipper.showNext();

		Game.getInstance().setActivity(this);
		Game.getInstance().loadTextures();

		GLSurfaceView glView = (GameGLSurfaceView) findViewById(R.id.glSurfaceViewID);
		glView.setEGLContextClientVersion(2);
		GameRenderer renderer = new GameRenderer();
		glView.setRenderer(renderer);

		Game.getInstance().setGlView(glView);
		Game.getInstance().setRenderer(renderer);

		// ************************************************************ //

		buttonReady = (Button) findViewById(R.id.button_ready);
		buttonReady.setAlpha(100);

		buttonR = (Button) findViewById(R.id.button_r);
		buttonR.setAlpha(100);

		healthText = (TextView) findViewById(R.id.text_hp);


		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {

				switch (msg.what) {
				case UPDATE_HEALT: {
					String text = (String) msg.obj;
					healthText.setText(text);
					break;
				}
				case INITIALIZED: {
					flipper.showPrevious();
					buttonReady.setVisibility(View.VISIBLE);
					break;
				}
				case READY: {
					layout_joystick.setVisibility(View.VISIBLE);
					layout_joystick_force.setVisibility(View.VISIBLE);
					buttonR.setVisibility(View.VISIBLE);
					buttonReady.setVisibility(View.INVISIBLE);
					break;
				}
				}
			}
		};
		
		if (Game.getInstance().getServer() != null) {
			Game.getInstance().getServer().addListener(new ServerAdapter() {
				@Override
				public void onClientReady(ServerEvent e) {
					if (Game.getInstance().getServer().getGameThread()
							.isEveryoneReady()) {

						Log.i(Game.TAG, "Server: trying start game : "
								+ Game.getInstance().getServer().tryStartGame());
					}
				}
			});
		}

		if(Game.getInstance().getClient() != null ) {
			
			buttonReady.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Game.getInstance().getClient().getGameThread().ready();
				}
			});

			buttonR.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Game.getInstance().getClient().getGameThread().resetPosition();

				}
			});
			
			Game.getInstance().getClient().addListener(new ClientAdapter() {
				@Override
				public void onGameInitialized(ClientEvent e) {
					handler.sendEmptyMessage(INITIALIZED);
				}
				
				@Override
				public void onClientReady(ClientEvent e) {
					handler.sendEmptyMessage(READY);
				}
			});
			
		}

		if (Game.getInstance().getServer() != null) {
			Game.getInstance().getServer().initializeGame();
		}

		layout_joystick = (RelativeLayout) findViewById(R.id.layout_joystick);

		joystic = new JoyStickClass(getApplicationContext(), layout_joystick,
				R.drawable.image_button);
		joystic.setStickSize(STICK_SIZE, STICK_SIZE);

		joystic.setLayoutAlpha(JOYSTICK_ALPHA);
		joystic.setStickAlpha(STICK_ALPHA);
		joystic.setOffset(0);
		joystic.setMinimumDistance(0);

		layout_joystick.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				joystic.drawStick(arg1);
				return true;
			}
		});

		layout_joystick_force = (RelativeLayout) findViewById(R.id.layout_joystick_force);

		joysticforce = new JoyStickClass(getApplicationContext(),
				layout_joystick_force, R.drawable.image_button);
		joysticforce.setStickSize(STICK_SIZE, STICK_SIZE);
		joysticforce.setLayoutAlpha(JOYSTICK_ALPHA);
		joysticforce.setStickAlpha(STICK_ALPHA);
		joysticforce.setOffset(0);
		joysticforce.setMinimumDistance(0);

		layout_joystick_force.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View arg0, MotionEvent arg1) {
				joysticforce.drawStick(arg1);
				return true;
			}			
		});
		
		layout_joystick.setVisibility(View.INVISIBLE);
		layout_joystick_force.setVisibility(View.INVISIBLE);
		buttonR.setVisibility(View.INVISIBLE);
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyUp(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		}
		return false;
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();

	}

	@Override
	protected void onDestroy() {
		if (Game.getInstance().getServer() != null) {
			Game.getInstance().getServer().stop();
			Game.getInstance().setServer(null);
		}
		if (Game.getInstance().getClient() != null) {
			Game.getInstance().getClient().disconnect();
			Game.getInstance().setClient(null);
		}
		Game.getInstance().unloadTextures();
		super.onDestroy();
	}

	public boolean onTouchEvent(MotionEvent me) {
		return super.onTouchEvent(me);
	}

	protected boolean isFullscreenOpaque() {
		return true;
	}

	public void setHealth(String health) {
		if (handler != null) {
			Message msg = new Message();
			msg.obj = health;
			msg.what = UPDATE_HEALT;
			handler.sendMessage(msg);
		}
	}
}

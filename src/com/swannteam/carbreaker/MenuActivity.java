package com.swannteam.carbreaker;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.swannteam.carbreaker.game.DefaultGameConfig;
import com.swannteam.carbreaker.game.Game;
import com.swannteam.carbreaker.network.Client;
import com.swannteam.carbreaker.network.NetworkInfo;
import com.swannteam.carbreaker.network.Server;

public class MenuActivity extends Activity {

	private static final int MAINMENU = 0;
	private static final int CONNECTSCREEN = 1;
	private static final int SERVERWAIT = 2;
	
	private Activity activity = this;
	private ViewFlipper flipper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.viewflipper);
		flipper = (ViewFlipper) findViewById(R.id.flipper);

		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		int layouts[] = new int[] { R.layout.mainmenu, R.layout.connectscreen,
				R.layout.serverwait};
		for (int layout : layouts)
			flipper.addView(inflater.inflate(layout, null));

		Button buttonConnect = (Button) findViewById(R.id.connectButton);
		Button buttonConnectstart = (Button) findViewById(R.id.connectButtonStart);
		Button buttonServer = (Button) findViewById(R.id.serverCreateButton);
		Button buttonStart = (Button) findViewById(R.id.startButton);

		TextView error = (TextView) findViewById(R.id.errorMsg);
		error.setVisibility(View.INVISIBLE);

		buttonServer.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Server server = new Server("CarBreaker Server");

				try {
					server.initialize(NetworkInfo.SERVER_PORT, NetworkInfo.DEFAULT_MAX_CLIENT);
					server.setGameConfig(new DefaultGameConfig());
					server.start();

					Game.getInstance().setServer(server);

					TextView servertext = (TextView) findViewById(R.id.serverCreatedText);
					servertext.append(" (" + getLocalIpAddress() + ")");

				} catch (IOException e) {
					e.printStackTrace();
				}
				flipper.showNext();
				flipper.showNext();

			}
		});

		buttonConnect.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				flipper.showNext();
			}
		});

		buttonConnectstart.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				TextView error = (TextView) findViewById(R.id.errorMsg);
				error.setVisibility(View.INVISIBLE);

				EditText edit = (EditText) findViewById(R.id.ipAddress);
				String ip = edit.getText().toString();

				if (ip.length() != 0) {

					Client client = new Client(NetworkInfo.DEFAULT_CLIENT_NAME);
					boolean res = client.connect(ip, NetworkInfo.CLIENT_PORT, NetworkInfo.SERVER_PORT);
					if (!res) {
						error.setVisibility(View.VISIBLE);
						return;
					}
					Game.getInstance().setClient(client);

					Intent intent = new Intent(activity, MainActivity.class);
					startActivity(intent);
				}
			}
		});

		buttonStart.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Game.getInstance().setClient(new Client(NetworkInfo.DEFAULT_CLIENT_NAME));
				if (Game.getInstance().getClient()
						.connect("localhost", NetworkInfo.CLIENT_PORT, NetworkInfo.SERVER_PORT)) {
					Intent intent = new Intent(activity, MainActivity.class);
					startActivity(intent);
				}

			}
		});

	}

	@Override
	public void onResume() {
		super.onResume();
		flipper.setDisplayedChild(MAINMENU);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyUp(keyCode, event);

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if ((flipper.getDisplayedChild() == CONNECTSCREEN)
					|| (flipper.getDisplayedChild() == SERVERWAIT)) {
				flipper.setDisplayedChild(MAINMENU);
				
				if (Game.getInstance().getServer() != null) {
					Game.getInstance().getServer().stop();
					Game.getInstance().setServer(null);
				}
				
				if (Game.getInstance().getClient() != null) {
					Game.getInstance().getClient().disconnect();
					Game.getInstance().setClient(null);
				}				
				
				return true;
			}
			finish();
		}
		return false;
	}

	public static String getLocalIpAddress() {
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface
					.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				for (Enumeration<InetAddress> enumIpAddr = intf
						.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()
							&& inetAddress instanceof Inet4Address) {
						return inetAddress.getHostAddress();
					}
				}
			}
		} catch (SocketException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}

package com.swannteam.carbreaker.world;

public interface IStrategy {
	void execute();
}

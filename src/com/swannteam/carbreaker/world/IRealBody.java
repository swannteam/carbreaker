package com.swannteam.carbreaker.world;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import com.threed.jpct.Object3D;

public interface IRealBody {

	void update();

	Object3D getObject3D();
	void setObject3D(Object3D object3d);

	RigidBody getRigidBody();
	void setRigidBody(RigidBody rigidBody);
	
	Transform getTransform();
	void setTransform(Transform transform);
	
	void setCommand(IStrategy command);
	IStrategy getCommand();
	void removeCommand();
	
	IConnection getConnection();
	void setConnection(IConnection connection);
}

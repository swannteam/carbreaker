package com.swannteam.carbreaker.world;

import java.util.ArrayList;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.vehicle.DefaultVehicleRaycaster;
import com.bulletphysics.dynamics.vehicle.RaycastVehicle;
import com.bulletphysics.dynamics.vehicle.VehicleRaycaster;
import com.bulletphysics.dynamics.vehicle.VehicleTuning;
import com.bulletphysics.dynamics.vehicle.WheelInfo;
import com.bulletphysics.dynamics.vehicle.WheelInfoConstructionInfo;
import com.bulletphysics.linearmath.Transform;
import com.swannteam.carbreaker.game.GameUtils;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;

public class Car extends RealBody implements IVehicle {

	private static final float wheelFriction = 5000f;
	private static final float suspensionStiffness = 700f;
	private static final float suspensionDamping = 300f;
	private static final float suspensionCompression = 10f;
	private static final float rollInfluence = 0.1f;

	private ArrayList<RealBody> wheelObjects = new ArrayList<RealBody>();
	private RaycastVehicle raycastVehicle;
	private VehicleTuning tuning;

	private static final Vector3f wheelDirection = new Vector3f(0, -1, 0);
	private static final Vector3f wheelAxle = new Vector3f(-1, 0, 0);
	
	private float health;
	private boolean alive = true;

	public Car(Object3D object3d, RigidBody carChassis, VehicleTuning tuning,
			VehicleRaycaster vehicleRayCaster) {
		super();

		this.rigidBody = carChassis;
		this.rigidBody.setUserPointer(this);
		this.object3d = object3d;
		this.tuning = tuning;
		rigidBody.setActivationState(CollisionObject.DISABLE_DEACTIVATION);
		raycastVehicle = new RaycastVehicle(tuning, rigidBody, vehicleRayCaster);
		raycastVehicle.setCoordinateSystem(0, 1, 2);
	}

	public boolean isAlive() {
		return alive;
	}
	
	public RaycastVehicle getRaycastVehicle() {
		return raycastVehicle;
	}

	public float getHealth() {
		return health;
	}

	public void setHealth(float health) {
		if(health <= 0) {
			this.health = 0;
			alive = false;
		} else {
			this.health = health;
		}
	}

	public void addWheel(RealWorld realWorld, Object3D wheelObject,
			Vector3f connectionPoint, float suspensionRestLength,
			float wheelRadius, boolean isFrontWheel) {
		RealBody wheel = new RealBody(wheelObject, null);
		realWorld.add(wheel);
		wheelObjects.add(wheel);
		raycastVehicle.addWheel(connectionPoint, wheelDirection, wheelAxle,
				suspensionRestLength, wheelRadius, tuning, isFrontWheel);
	}

	@Override
	public void update() {
		if (command != null) {
			command.execute();
		}
		if (connection != null)
			connection.setGraphicFromBody(this);

		for (int i = 0; i < raycastVehicle.getNumWheels(); ++i) {
			raycastVehicle.updateWheelTransform(i, true);
			if (connection != null) {
				connection.setGraphicFromTransform(wheelObjects.get(i)
						.getObject3D(),
						raycastVehicle.getWheelInfo(i).worldTransform);
			}
		}
	}

	float steering;
	float force;
	float breakforce;

	public void setData(float steering, float force, float breakforce) {
		this.steering = steering;
		this.force = force;
		this.breakforce = breakforce;

		raycastVehicle.setSteeringValue(steering, 0);
		raycastVehicle.setSteeringValue(steering, 1);

		raycastVehicle.applyEngineForce(force, 0);
		raycastVehicle.applyEngineForce(force, 1);
		raycastVehicle.setBrake(breakforce, 0);
		raycastVehicle.setBrake(breakforce, 1);
	}

	
	
	@Override
	public void getData(float[] out) {
		out[0] = steering;
		out[1] = force;
		out[2] = breakforce;
	}

	public void setupDefaultWheels(RealWorld realWorld, Object3D wheelObject,
			Vector3f[] connectionPoints, float suspensionRestLength,
			float wheelRadius) {

		Transform wheeltr1 = new Transform();
		wheeltr1.setIdentity();
		// wheeltr1.origin.set(new Vector3f(0, 0, 0));

		Transform wheeltr2 = new Transform();
		wheeltr2.setIdentity();
		// wheeltr2.origin.set(new Vector3f(0, 0, 0));

		Transform wheeltr3 = new Transform();
		wheeltr3.setIdentity();
		// wheeltr3.origin.set(new Vector3f(0, 0, 0));

		Transform wheeltr4 = new Transform();
		wheeltr4.setIdentity();
		// wheeltr4.origin.set(new Vector3f(0, 0, 0));

		Object3D wheel1 = null;
		Object3D wheel2 = null;
		Object3D wheel3 = null;
		Object3D wheel4 = null;

		if (wheelObject != null) {
			wheel1 = new Object3D(wheelObject);
			wheel2 = new Object3D(wheelObject);
			wheel3 = new Object3D(wheelObject);
			wheel4 = new Object3D(wheelObject);

			wheel1.rotateZ((float) Math.PI / 2f);
			wheel1.rotateMesh();
			wheel1.clearRotation();

			wheel2.rotateZ((float) Math.PI / 2f);
			wheel2.rotateMesh();
			wheel2.clearRotation();

			wheel3.rotateZ(-(float) Math.PI / 2f);
			wheel3.rotateMesh();
			wheel3.clearRotation();

			wheel4.rotateZ(-(float) Math.PI / 2f);
			wheel4.rotateMesh();
			wheel4.clearRotation();

		}

		boolean isFrontWheel = true;

		//getRaycastVehicle().setCoordinateSystem(0, 3, 0);

		addWheel(realWorld, wheel1, connectionPoints[0], suspensionRestLength,
				wheelRadius, isFrontWheel);

		addWheel(realWorld, wheel2, connectionPoints[1], suspensionRestLength,
				wheelRadius, isFrontWheel);

		isFrontWheel = false;

		addWheel(realWorld, wheel3, connectionPoints[2], suspensionRestLength,
				wheelRadius, isFrontWheel);

		addWheel(realWorld, wheel4, connectionPoints[3], suspensionRestLength,
				wheelRadius, isFrontWheel);

		for (int i = 0; i < raycastVehicle.getNumWheels(); ++i) {
			WheelInfo wheelInfo = raycastVehicle.getWheelInfo(i);

			wheelInfo.suspensionStiffness = suspensionStiffness;
			wheelInfo.wheelsDampingRelaxation = suspensionDamping;
			wheelInfo.wheelsDampingCompression = suspensionCompression;
			wheelInfo.frictionSlip = wheelFriction;
			wheelInfo.rollInfluence = rollInfluence;
		}
	}

}

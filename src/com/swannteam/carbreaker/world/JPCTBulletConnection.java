package com.swannteam.carbreaker.world;

import com.bulletphysics.linearmath.MatrixUtil;
import com.bulletphysics.linearmath.Transform;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;

public class JPCTBulletConnection implements IConnection {

	public final Transform centerOfMassOffset = new Transform();
	private float[] matDump;
	private Matrix mat;

	public JPCTBulletConnection() {
		centerOfMassOffset.setIdentity();
		matDump = new float[16];
		mat = new Matrix();
	}

	@Override
	public synchronized void setGraphicFromTransform(Object3D object3d, Transform transform) {
		if (object3d != null && transform != null) {
			SimpleVector pos = SimpleVector.create();
			object3d.getTransformedCenter(pos);
			object3d.translate((transform.origin.x - pos.x), (-transform.origin.y
					- pos.y), (-transform.origin.z - pos.z));

			Matrix matrix = object3d.getRotationMatrix();
			matrix.fillDump(matDump);
			MatrixUtil.getOpenGLSubMatrix(transform.basis, matDump);

			mat.setDump(matDump);
			mat.rotateX((float) Math.PI);
			object3d.setRotationMatrix(mat);

			mat = new Matrix();
		}
	}

	@Override
	public void setGraphicFromBody(IRealBody realBody) {
		setGraphicFromTransform(realBody.getObject3D(), realBody.getTransform());

	}

	@Override
	public void setBodyFromGraphic(IRealBody realBody) {
		// TODO Auto-generated method stub

	}

}
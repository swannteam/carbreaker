package com.swannteam.carbreaker.world;

import com.bulletphysics.dynamics.vehicle.RaycastVehicle;

public interface IVehicle extends IRealBody {
	RaycastVehicle getRaycastVehicle();
	void setData(float steering, float force, float breakforce);
	void getData(float[] out);
}

package com.swannteam.carbreaker.world;

import java.util.ArrayList;

import javax.vecmath.Vector3f;

import android.nfc.cardemulation.OffHostApduService;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.dynamics.vehicle.RaycastVehicle;
import com.bulletphysics.linearmath.MatrixUtil;
import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.World;
import com.threed.jpct.util.SkyBox;

public class RealWorld {

	private ArrayList<IRealBody> realBodies = new ArrayList<IRealBody>();
	private ArrayList<IVehicle> vehicles = new ArrayList<IVehicle>();
	private DiscreteDynamicsWorld dynamicsWorld;
	private World world;
	private SkyBox sky;

	private Object3D followedObject = null;
	private Camera camera = null;
	private SimpleVector cameraOffset = null;

	private IConnection connection;

	/***********************************************************************/

	private float timeStep = 1.f / 120.f;
	private int maxSubSteps = 1;

	/***********************************************************************/

	public RealWorld() {
	}

	public RealWorld(DiscreteDynamicsWorld dynamicsWorld, World world) {
		this.dynamicsWorld = dynamicsWorld;
		this.world = world;
	}

	public DiscreteDynamicsWorld getDynamicsWorld() {
		return dynamicsWorld;
	}

	public void setDynamicsWorld(DiscreteDynamicsWorld dynamicsWorld) {
		this.dynamicsWorld = dynamicsWorld;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	public SkyBox getSky() {
		return sky;
	}

	public void setSky(SkyBox sky) {
		this.sky = sky;
	}

	public ArrayList<IRealBody> getRealBodies() {
		return realBodies;
	}

	public ArrayList<IVehicle> getVehicles() {
		return vehicles;
	}

	public IConnection getConnection() {
		return connection;
	}

	public void setConnection(IConnection connection) {
		this.connection = connection;
	}

	public void setStepParameters(float timeStep, int maxSubSteps) {
		this.timeStep = timeStep;
		this.maxSubSteps = maxSubSteps;
	}

	private void addInside(IRealBody body) {
		if (body.getConnection() == null)
			body.setConnection(connection);

		if (body.getObject3D() != null) {
			body.getObject3D().strip();
			body.getObject3D().build();
			if (world != null) {
				world.addObject(body.getObject3D());
			}
		}

		if (body.getRigidBody() != null)
			dynamicsWorld.addRigidBody(body.getRigidBody());

	}

	public boolean add(IRealBody body) {
		addInside(body);
		return realBodies.add(body);
	}

	public void add(int index, IRealBody body) {
		addInside(body);
		realBodies.add(index, body);
	}

	public void addVehicle(Car car) {
		if (car != null && car.getRaycastVehicle() != null) {
			car.setConnection(connection);
			vehicles.add(car);
			if (world != null) {
				world.addObject(car.getObject3D());
			}
			if (dynamicsWorld != null) {
				dynamicsWorld.addRigidBody(car.getRigidBody());
				dynamicsWorld.addVehicle(car.getRaycastVehicle());
			}
		}
	}

	public void setCameraToBody(IRealBody body, SimpleVector offset) {
		if (world != null) {
			camera = world.getCamera();
			followedObject = body.getObject3D();
			cameraOffset = offset;
			camera.setEllipsoidMode(Camera.ELLIPSOID_TRANSFORMED);
		}
	}

	SimpleVector oldOffset = new SimpleVector();
	SimpleVector newOffset = new SimpleVector();
	SimpleVector deltaOffset = new SimpleVector();

	private void updateCamera() {
		if (followedObject != null) {

			Matrix mat = followedObject.getRotationMatrix().cloneMatrix();
			newOffset = new SimpleVector(cameraOffset);

			newOffset.rotate(mat);

			deltaOffset = newOffset.calcSub(oldOffset);
			deltaOffset.scalarMul(0.05f);
			newOffset = oldOffset.calcAdd(deltaOffset);

			newOffset.y = cameraOffset.y;

			camera.setPosition(followedObject.getTransformedCenter().calcAdd(
					newOffset));
			camera.lookAt(followedObject.getTransformedCenter());

			oldOffset = newOffset;
		}
	}

	public IRealBody get(int index) {
		return realBodies.get(index);
	}

	private long time_physics_prev = 0;
	private long time_physics_curr = 1;

	public void tick() {

		time_physics_curr = System.currentTimeMillis();

		if (dynamicsWorld != null) {
			dynamicsWorld.stepSimulation(
			// (float) ((time_physics_curr - time_physics_prev) / 500.0),
					timeStep, maxSubSteps);
		}

		time_physics_prev = time_physics_curr;
	}

	public void draw(FrameBuffer frameBuffer) {

		if (world != null) {
			for (IRealBody body : realBodies) {
				if (body.getRigidBody() != null
						&& !body.getRigidBody().isStaticObject())
					body.update();
			}

			for (IVehicle vehicle : vehicles) {
				vehicle.update();
			}

			updateCamera();
			frameBuffer.clear();
			if (sky != null) {
				sky.render(world, frameBuffer);
			}
			world.renderScene(frameBuffer);
			world.draw(frameBuffer);
			frameBuffer.display();
		}
	}

	public void resetDynamicWorld() {

		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();
		CollisionDispatcher dispatcher = new CollisionDispatcher(
				collisionConfiguration);

		Vector3f worldAabbMin = new Vector3f(-1000, -1000, -1000);
		Vector3f worldAabbMax = new Vector3f(1000, 1000, 1000);
		int maxProxies = 1024;

		AxisSweep3 overlappingPairCache = new AxisSweep3(worldAabbMin,
				worldAabbMax, maxProxies);

		// BroadphaseInterface overlappingPairCache = new DbvtBroadphase();

		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		DiscreteDynamicsWorld dynamicsWorld = new DiscreteDynamicsWorld(
				dispatcher, overlappingPairCache, solver,
				collisionConfiguration);

		dynamicsWorld.setGravity(new Vector3f(0, -10f, 0));

		this.dynamicsWorld = dynamicsWorld;
		connection = new JPCTBulletConnection();

	}

	public void resetGraphicWorld() {
		world = new World();

		world.setAmbientLight(0, 0, 0);
		Light sun = new Light(world);
		sun.setIntensity(250, 250, 250);
		sun.setPosition(new SimpleVector(0, -1000, 0));
	}
}

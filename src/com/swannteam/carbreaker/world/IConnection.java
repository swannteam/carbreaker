package com.swannteam.carbreaker.world;

import com.bulletphysics.linearmath.Transform;
import com.threed.jpct.Object3D;

public interface IConnection {
	//IConnection getUniqueInstance();
	
	void setGraphicFromTransform(Object3D object3d, Transform transform);
	
	void setGraphicFromBody(IRealBody realBody);
	void setBodyFromGraphic(IRealBody realBody);
}

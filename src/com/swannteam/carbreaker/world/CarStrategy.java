package com.swannteam.carbreaker.world;

public class CarStrategy implements IStrategy {

	private Car car;
	
	private float steering;
	private float force;
	private float breakforce;
	
	public CarStrategy(Car car) {
		this.car = car;
	}

	@Override
	public void execute() {
		car.getRaycastVehicle().setSteeringValue(steering, 0);
		car.getRaycastVehicle().setSteeringValue(steering, 1);
		
		car.getRaycastVehicle().applyEngineForce(force, 0);
		car.getRaycastVehicle().applyEngineForce(force, 1);
		car.getRaycastVehicle().setBrake(breakforce, 0);
		car.getRaycastVehicle().setBrake(breakforce, 1);
	}

	public void setParameters(float steering, float force, float breakforce) {
		this.steering = steering;
		this.force = force;
		this.breakforce = breakforce;
	}
	
}

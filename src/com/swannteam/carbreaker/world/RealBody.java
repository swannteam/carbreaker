package com.swannteam.carbreaker.world;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.linearmath.Transform;
import com.threed.jpct.Object3D;

public class RealBody implements IRealBody {

	protected RigidBody rigidBody;
	protected Object3D object3d;
	protected IStrategy command = null;
	protected IConnection connection = null;

	protected Transform bufferTransform = new Transform();
	
	public RealBody() {}
	
	public RealBody(Object3D object3d, RigidBody rigidBody) {
		this.rigidBody = rigidBody;
		this.object3d = object3d;
	}

	@Override
	public void update() {
		if(command != null)
			command.execute();
		
		if(connection != null)
			connection.setGraphicFromBody(this);
	}

	@Override
	public Object3D getObject3D() {
		return object3d;
	}

	@Override
	public void setObject3D(Object3D object3d) {
		this.object3d = object3d;
	}

	@Override
	public RigidBody getRigidBody() {
		return rigidBody;
	}
	
	@Override
	public void setRigidBody(RigidBody rigidBody) {
		this.rigidBody = rigidBody;
	}

	@Override
	public Transform getTransform() {
		rigidBody.getMotionState().getWorldTransform(bufferTransform);
		return bufferTransform;
	}

	@Override
	public void setTransform(Transform transform) {
		rigidBody.setWorldTransform(transform);
	}

	@Override
	public IConnection getConnection() {
		return connection;
	}

	@Override
	public void setConnection(IConnection connection) {
		this.connection = connection;
	}
	
	@Override
	public void setCommand(IStrategy command) {
		this.command = command;
		
	}

	@Override
	public IStrategy getCommand() {
		return command;
	}

	@Override
	public void removeCommand() {
		command = null;
		
	}

}

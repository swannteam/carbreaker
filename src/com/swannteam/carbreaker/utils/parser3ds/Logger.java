package com.swannteam.carbreaker.utils.parser3ds;

/**
 * Logger interface to connect your own logger to the Parser
 *
 * @author Kjetil Г�sterГҐs
 */
public interface Logger {

    void log(String s);

}

package com.swannteam.carbreaker.utils.matrix;

import javax.vecmath.Matrix3f;

public class ArrayUtil {

	public static float[] getArrayFrom(Matrix3f matrix) {
		float[] arr = new float[9];
		
		arr[0] = matrix.m00;
		arr[1] = matrix.m01;
		arr[2] = matrix.m02;
		
		arr[3] = matrix.m10;
		arr[4] = matrix.m11;
		arr[5] = matrix.m12;
		
		arr[6] = matrix.m20;
		arr[7] = matrix.m21;
		arr[8] = matrix.m22;
		
		return arr;		
	}

}

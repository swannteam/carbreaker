package com.swannteam.carbreaker.network;

import java.util.concurrent.ConcurrentLinkedQueue;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import android.util.Log;

import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.vehicle.DefaultVehicleRaycaster;
import com.bulletphysics.dynamics.vehicle.VehicleTuning;
import com.bulletphysics.linearmath.Transform;
import com.swannteam.carbreaker.game.DefaultGameConfig;
import com.swannteam.carbreaker.game.Game;
import com.swannteam.carbreaker.game.GameUtils;
import com.swannteam.carbreaker.game.IGameConfig;
import com.swannteam.carbreaker.network.events.ClientEventSource;
import com.swannteam.carbreaker.world.Car;
import com.swannteam.carbreaker.world.IVehicle;
import com.swannteam.carbreaker.world.RealBody;
import com.swannteam.carbreaker.world.RealWorld;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.util.MemoryHelper;
import com.threed.jpct.util.SkyBox;

public class GameClientThread extends Thread {

	private static final int MAX_MESSAGES_PER_TICK = 20;

	private ConcurrentLinkedQueue<GameMessage> queue = new ConcurrentLinkedQueue<GameMessage>();

	protected boolean ready = false;
	protected boolean initialized = false;

	private RealWorld realWorld;
	private Client client;

	private byte myId = 0;

	private float wheelWidth = 0.2f;
	private SimpleVector cameraOffset = new SimpleVector(-0, -7, -25);

	private Transform transform = new Transform();
	private ClientEventSource eventSource;
	private GameMessageHelper msgHelper = new GameMessageHelper();

	public void draw(FrameBuffer frameBuffer) {
		synchronized (realWorld) {
			realWorld.draw(frameBuffer);
		}
	}

	public boolean ready() {
		if (initialized) {

			GameMessage msg = new GameMessage();
			msgHelper.setGameData(msg, CommandType.READY, 0);

			client.send(msg.getServerPacket());

			ready = true;
			eventSource.fireEventClientReady(client, "", 0);

			return true;
		}
		return false;
	}

	public byte getMyId() {
		return myId;
	}

	public GameClientThread(Client client, ClientEventSource eventSource) {
		this.client = client;
		this.eventSource = eventSource;
	}

	public void init(IGameConfig gameConfig, byte players, byte myId) {

		// RealWorld

		realWorld = new RealWorld();
		realWorld.resetDynamicWorld();
		realWorld.resetGraphicWorld();

		if (!gameConfig.initialize(Game.getInstance().getActivity())) {
			// TODO error
			return;
		}

		this.myId = myId;

		// World

		SkyBox sky = new SkyBox("left", "front", "right", "back", "up", "down",
				300);
		sky.setCenter(new SimpleVector(0, 100, 0));
		realWorld.setSky(sky);

		Object3D ground = gameConfig.getGroundGraphics();

		Object3D groundmount = gameConfig.getMountGraphics();

		ground.setTexture("stone.jpg");
		groundmount.setTexture("grass.jpg");
		ground.clearShader();
		ground.build();

		float mass = 0;
		Transform startTransform = new Transform();
		startTransform.setIdentity();
		startTransform.origin.set(0, 0, 0);

		realWorld.add(new RealBody(ground, GameUtils.CreateRigidBodyFromShape(
				gameConfig.getGroundCollisionShape(), mass, new Vector3f(0, 0,
						0), startTransform)));

		realWorld.add(new RealBody(groundmount, GameUtils
				.CreateRigidBodyFromShape(gameConfig.getGroundCollisionShape(),
						mass, new Vector3f(0, 0, 0), startTransform)));

		// Cars

		Vector3f[] connectionPoints = gameConfig.getConnectionPoints();
		float wheelRadius = gameConfig.getWheelRadius();
		float suspensionRestLength = gameConfig.getSuspensionRestLength();

		for (int i = 0; i < players; ++i) {
			RigidBody carBody = GameUtils.CreateRigidBodyFromShape(
					gameConfig.getCarCollisionShape(), gameConfig.getCarMass(),
					new Vector3f(0, 0, 0), gameConfig.getStartTransforms()[i]);

			Car car = new Car(new Object3D(gameConfig.getCarGraphics()),
					carBody, new VehicleTuning(), new DefaultVehicleRaycaster(
							realWorld.getDynamicsWorld()));

			car.getRigidBody().setCenterOfMassTransform(
					gameConfig.getCentersOfMass()[i]);

			realWorld.addVehicle(car);

			car.setupDefaultWheels(
					realWorld,
					Primitives.getCylinder(10, wheelRadius, wheelWidth
							/ (2 * wheelRadius)), connectionPoints,
					suspensionRestLength, wheelRadius);
		}

		realWorld.setCameraToBody(realWorld.getVehicles().get(myId),
				cameraOffset);
		MemoryHelper.compact();

		Log.i(Game.TAG, "Client: game initialized");
	}

	public void push(GameMessage message) {
		switch (message.getCommandType()) {
		case GO: {
			if (ready) {
				Log.i(Game.TAG, "Client: GO");
				start();
			}
			break;
		}
		case EXIT: {
			if (isAlive()) {
				Thread.currentThread().interrupt();
				eventSource.fireEventGameFinished(client, "", 0);
			}
			break;
		}
		case INIT: {
			byte[] data = message.getData();
			switch (data[0]) {
			case 0: {
				init(new DefaultGameConfig(), data[1], message.getInfo()[5]);
				break;
			}
			default: {
				return;
			}
			}

			initialized = true;
			eventSource.fireEventGameInitialized(client, "", 0);
			break;
		}
		default: {
			queue.add(message);
			break;
		}
		}
	}

	public void apply(GameMessage message) {

		switch (message.getCommandType()) {
		case TRANSFORM: {
			msgHelper.getTransform(message, transform);
			byte id = message.info[5];

			synchronized (realWorld) {
				realWorld.getVehicles().get(id).setTransform(transform);
				// realWorld.getVehicles().get(id).update();
			}
			break;
		}
		case CAR_DATA: {
			float[] data = message.getFloatArray();
			byte id = message.info[5];

			synchronized (realWorld) {
				realWorld.getVehicles().get(id)
						.setData(data[0], data[1], data[2]);
				// realWorld.getVehicles().get(id).update();
			}
			break;
		}
		case CAR_HEALTH: {
			float[] data = message.getFloatArray();
			byte id = message.info[5];

			synchronized (realWorld) {
				((Car) realWorld.getVehicles().get(id)).setHealth(data[0]);
				// realWorld.getVehicles().get(id).update();
			}
			break;
		}
		default: {
			break;
		}
		}
	}

	private GameMessage msg;

	public void run() {
		Log.i(Game.TAG, "Client: game thread started");

		if (!ready) {
			return;
		}

		try {
			while ((!Thread.currentThread().isInterrupted())) {
				if (realWorld != null) {
					synchronized (realWorld) {
						for (int i = 0; i < MAX_MESSAGES_PER_TICK; ++i) {
							msg = queue.poll();
							if (msg == null) {
								break;
							}
							apply(msg);

						}

						realWorld.tick();
					}
				}

				Thread.sleep(1);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		Log.i(Game.TAG, "Client: game thread finished");
	}

	public boolean isMyCarAlive() {
		return ((Car) realWorld.getVehicles().get(myId)).isAlive();
	}

	public void setCarData(float steering, float engineForce, float breakForce) {

		if (!isMyCarAlive()) {
			return;
		}

		GameMessage msg = new GameMessage();
		msgHelper.setCarData(msg, steering, engineForce, breakForce);
		msgHelper.setGameData(msg, CommandType.CAR_DATA);

		client.send(msg.getServerPacket());
		synchronized (realWorld) {
			realWorld.getVehicles().get(myId)
					.setData(steering, engineForce, breakForce);
		}
	}

	public float getHealth() {
		return ((Car) realWorld.getVehicles().get(myId)).getHealth();
	}

	public void resetPosition() {

		if (!isMyCarAlive()) {
			return;
		}

		GameMessage msg = new GameMessage();
		msgHelper.setGameData(msg, CommandType.RESET_POSITION);
		client.send(msg.getServerPacket());

		IVehicle mycar = realWorld.getVehicles().get(myId);

		mycar.getRigidBody().setLinearVelocity(new Vector3f(0, 0, 0));
		mycar.getRigidBody().setAngularVelocity(new Vector3f(0, 0, 0));
	}

}

package com.swannteam.carbreaker.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.os.AsyncTask;
import android.util.Log;

import com.swannteam.carbreaker.game.Game;
import com.swannteam.carbreaker.network.ServerPacket.Type;
import com.swannteam.carbreaker.network.events.ClientEventSource;
import com.swannteam.carbreaker.network.events.ClientListener;
import com.threed.jpct.FrameBuffer;

public class Client {

	private ClientEventSource eventSource = new ClientEventSource();

	private Sender sender;
	
	private DatagramSocket clientSocket = null;
	private InetAddress hostAddress = null;

	private String clientName;
	private String serverName;

	private GameClientThread gameThread;
	private ClientThread clientThread;

	protected String host;
	protected int myPort;
	protected int hostPort;

	protected boolean connected = false;

	/***********************************************************************************************/

	public Client(String clientName) {
		this.gameThread = new GameClientThread(this, eventSource);
		this.clientName = clientName;
	}

	public void addListener(ClientListener listener) {
		eventSource.addEventListener(listener);
	}

	public void removeListener(ClientListener listener) {
		eventSource.removeEventListener(listener);
	}

	public boolean isConnected() {
		return connected;
	}

	public String getHost() {
		return host;
	}

	public int getMyPort() {
		return myPort;
	}

	public int getHostPort() {
		return myPort;
	}
	
	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public DatagramSocket getClientSocket() {
		return clientSocket;
	}

	public String getServerName() {
		return serverName;
	}

	public GameClientThread getGameThread() {
		return gameThread;
	}

	public void setGameThread(GameClientThread gameThread) {
		this.gameThread = gameThread;
	}

	public boolean isInitialized() {
		return gameThread.initialized;
	}

	/***********************************************************************************************/

	public boolean connect(String host, int myPort, int hostPort) {
		try {
			this.myPort = myPort;
			this.hostPort = hostPort;
			
			clientSocket = new DatagramSocket(myPort);
			sender = new Sender(clientSocket);
			sender.start();
			
			ConnectionTask task = new ConnectionTask(host);
			task.execute();

			serverName = task.get();
			
			if(serverName == null) {
				return (connected = false);
			}

			clientThread = new ClientThread(this);
			clientThread.start();

			Log.i(Game.TAG, "Client: connected to the '" + serverName + "'");
			
			eventSource.fireEventClientAccepted(this, "", 0);

			return (connected = true);
		} catch (Exception e) {
			return (connected = false);
		}
	}

	private class ConnectionTask extends AsyncTask<Void, Void, String> {
		String host;

		protected ConnectionTask(String host) {
			this.host = host;
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				hostAddress = InetAddress.getByName(host);
				
				ServerPacket srvPack = new ServerPacket(Type.REGISTER,
						clientName.getBytes());

				send(srvPack);

				byte[] buffer = new byte[ServerPacket.PACKAGE_SIZE];
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

				clientSocket.receive(packet);
				ServerPacket receivedPack = new ServerPacket(packet);
				
				if (receivedPack.getType() != Type.REGISTER) {
					return null;
				}
				
				return new String(receivedPack.getData());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	public void disconnect() {
		if (clientSocket != null) {
			clientThread.interrupt();
			clientSocket.close();

			eventSource.fireEventClientDisconnected(this, "", 0);
		}
	}

	public void send(ServerPacket packet) {
		packet.setAddress(hostAddress);
		packet.setPort(hostPort);
		sender.push(packet.createPacket());
		//Log.i(Game.TAG, "Client: client has send a message");
	}

	public void draw(FrameBuffer frameBuffer) {
		gameThread.draw(frameBuffer);
	}

	public boolean isReady() {
		return gameThread.ready;
	}

	/************************************************************************************************/

	long time1;
	long time2;
	
	public class ClientThread extends Thread {

		private byte[] buffer = new byte[ServerPacket.PACKAGE_SIZE];
		private DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		private ServerPacket serverPacket = new ServerPacket();
		private Client client;

		public ClientThread(Client client) {
			this.client = client;
		}

		public void run() {
			try {

				Log.i(Game.TAG, "Client: thread start");

				while ((!Thread.currentThread().isInterrupted())) {

					packet = new DatagramPacket(buffer, buffer.length);
					
					clientSocket.receive(packet);

					if (!ServerPacket.isValidArray(buffer)) {
						continue;
					}

					serverPacket.set(packet);

					switch (serverPacket.getType()) {

					case DATA: {
						gameThread.push(new GameMessage(serverPacket));
						break;
					}
					default: {
						break;
					}
					}
					
					Thread.sleep(1);
				}

			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (gameThread.isAlive()) {
				gameThread.interrupt();
			}

			eventSource.fireEventClientDisconnected(client, "", 0);
			
			Log.i(Game.TAG, "Client: thread finished");
		}
	}
}
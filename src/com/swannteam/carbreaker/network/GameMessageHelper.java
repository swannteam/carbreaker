package com.swannteam.carbreaker.network;

import java.nio.ByteBuffer;

import com.bulletphysics.linearmath.Transform;

public class GameMessageHelper {

	public static final int TRANSFORM_FLOATS = 12;
	public static final int CAR_DATA_FLOATS = 3;
	public static final int INIT_BYTES = 3;
	public static final int CAR_HEALTH_FLOATS = 1;
	
	public GameMessageHelper() {
	}
	
	public void getTransform(GameMessage message, Transform out) {
 		
		float[] data = message.getFloatArray();
		
		if(data.length < TRANSFORM_FLOATS) {
			return;
		}

		//Transform out = new Transform();
		//out.setIdentity();
		
		out.origin.x = data[0];
		out.origin.y = data[1];
		out.origin.z = data[2];
		
		out.basis.m00 = data[3];
		out.basis.m01 = data[4];
		out.basis.m02 = data[5];

		out.basis.m10 = data[6];
		out.basis.m11 = data[7];
		out.basis.m12 = data[8];

		out.basis.m20 = data[9];
		out.basis.m21 = data[10];
		out.basis.m22 = data[11];
	}
	
	public void setTransform(GameMessage message, Transform transform) {
		float[] data = new float[TRANSFORM_FLOATS];
		
		data[0] = transform.origin.x;
		data[1] = transform.origin.y; 
		data[2] = transform.origin.z; 

		data[3] = transform.basis.m00; 
		data[4] = transform.basis.m01; 
		data[5] = transform.basis.m02; 

		data[6] = transform.basis.m10; 
		data[7] = transform.basis.m11; 
		data[8] = transform.basis.m12; 

		data[9] = transform.basis.m20; 
		data[10] = transform.basis.m21; 
		data[11] = transform.basis.m22; 
		
		message.setData(data);
	}
	
	public void setCarData(GameMessage message, float steering, float engineForce, float breakForce) {
		float[] data = new float[CAR_DATA_FLOATS];

		data[0] = steering;
		data[1] = engineForce;
		data[2] = breakForce;
		
		message.setData(data);
	}
	
	public void setCarHealth(GameMessage message, float health) {
		float[] data = new float[CAR_HEALTH_FLOATS];

		data[0] = health;
		
		message.setData(data);
	}
	
	public void setGameData(GameMessage message, CommandType type) {
		byte[] info = new byte[ServerPacket.DATA_SECTION];
		
		info[0] = (byte)ServerPacket.Type.DATA.ordinal();
		info[1] = (byte)type.ordinal();
		
		message.setInfo(info);
	}
	
	public void setGameData(GameMessage message, CommandType type, int id) {
		byte[] info = new byte[ServerPacket.DATA_SECTION];
		
		info[0] = (byte)ServerPacket.Type.DATA.ordinal();
		info[1] = (byte)type.ordinal();

		byte[] userId = ByteBuffer.allocate(4).putInt(id).array();
		System.arraycopy(userId, 0, info, 2, 4);
		
		message.setInfo(info);
	}
	
	public void setGameData(GameMessage message, CommandType type, byte id) {
		byte[] info = new byte[ServerPacket.DATA_SECTION];
		
		info[0] = (byte)ServerPacket.Type.DATA.ordinal();
		info[1] = (byte)type.ordinal();
		info[5] = id;
		
		message.setInfo(info);
	}

}

package com.swannteam.carbreaker.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import android.os.Debug;
import android.util.Log;

import com.swannteam.carbreaker.game.DefaultGameConfig;
import com.swannteam.carbreaker.game.Game;
import com.swannteam.carbreaker.game.IGameConfig;
import com.swannteam.carbreaker.network.Server.ServerClient;
import com.swannteam.carbreaker.network.ServerPacket.Type;
import com.swannteam.carbreaker.network.events.ServerEventSource;
import com.swannteam.carbreaker.network.events.ServerListener;

public class Server {

	protected DatagramSocket serverSocket;
	protected HashMap<InetAddress, ServerClient> serverClients = new HashMap<InetAddress, ServerClient>();
	private Sender sender;

	protected int port;
	protected int maxClientsCount;
	protected String serverName;

	private boolean registrate = true;

	private ServerEventSource eventSource = new ServerEventSource();
	private ServerThread serverThread;
	private GameServerThread gameThread;
	private IGameConfig gameConfig;
	private GameMessageHelper msgHelper = new GameMessageHelper();

	private boolean initialized;

	public Server(String serverName) {
		this.serverName = serverName;
		serverThread = new ServerThread(this);
		gameThread = new GameServerThread(this, eventSource);
	}

	/*
	 * Getters and setters
	 */

	public GameServerThread getGameThread() {
		return gameThread;
	}

	public void addListener(ServerListener listener) {
		eventSource.addEventListener(listener);
	}

	public void removeListener(ServerListener listener) {
		eventSource.removeEventListener(listener);
	}

	public boolean canRegister() {
		return registrate;
	}

	public void enableRegistration() {
		registrate = true;
	}

	public void disableRegistration() {
		registrate = false;
	}

	public int getMaxClientsCount() {
		return maxClientsCount;
	}

	public void setMaxClientsCount(int maxClients) {
		this.maxClientsCount = maxClients;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public DatagramSocket getServerSocket() {
		return serverSocket;
	}

	public int getPort() {
		return port;
	}

	public IGameConfig getGameConfig() {
		return gameConfig;
	}

	public void setGameConfig(IGameConfig gameConfig) {
		this.gameConfig = gameConfig;
		maxClientsCount = gameConfig.getMaxPlayers();
	}

	/*
	 * Main methods
	 */

	public void initializeGame() {
		// serverThread.interrupt();

		if (!gameThread.initialize(gameConfig)) {
			return;
		}

		byte gameConfigType = -1;

		if (gameConfig instanceof DefaultGameConfig) {
			gameConfigType = 0;
		}

		// else if ...

		GameMessage msg = new GameMessage();
		msg.setData(new byte[] { gameConfigType, (byte) serverClients.size() });

		for (ServerClient srvClient : serverClients.values()) {
			msgHelper.setGameData(msg, CommandType.INIT,
					gameThread.players.get(srvClient.address.hashCode()));
			send(srvClient, msg.getServerPacket());
		}

		initialized = true;

		eventSource.fireEventGameInitialized(this, "", 0);
	}

	public void initialize(int port, int maxClients) throws SocketException {
		if (serverSocket != null && !serverSocket.isClosed()) {
			serverSocket.close();
		}

		serverSocket = new DatagramSocket(port);
		sender = new Sender(serverSocket);
		sender.start();

		this.port = port;
		this.maxClientsCount = maxClients;

		Log.i(Game.TAG, "Server: initialized");
	}

	public boolean tryStartGame() {
		if (gameThread.isEveryoneReady() && initialized) {
			gameThread.start();

			GameMessage msg = new GameMessage();
			msgHelper.setGameData(msg, CommandType.GO, 0);
			sendToAll(msg.getServerPacket());

			Log.i(Game.TAG, "Server: GO sended");

			eventSource.fireEventGameStarted(this, "", 0);
			return true;
		}
		return false;
	}

	public void finishGame() {
		if (gameThread.isAlive()) {
			gameThread.interrupt();

			eventSource.fireEventGameFinished(this, "", 0);
		}
	}

	public boolean start() {

		if (serverSocket != null) {
			serverThread.start();

			return true;
		}
		return false;
	}

	public void stop() {
		finishGame();
		
		if (serverThread.isAlive()) {
			serverThread.interrupt();
		}
		if (sender != null && sender.isAlive()) {
			sender.interrupt();
		}
		if (serverSocket != null && !serverSocket.isClosed()) {
			serverSocket.close();
		}
	}

	public void send(ServerClient serverClient, ServerPacket serverPacket) {
		if (sender != null) {
			sender.push(serverPacket.createPacket(serverClient));
		}
	}

	public void send(ServerPacket serverPacket) {
		if (sender != null) {
			sender.push(serverPacket.createPacket());
		}
	}

	public void sendToAll(ServerPacket serverPacket) {
		if (serverSocket != null) {
			for (ServerClient serverClient : serverClients.values()) {
				sender.push(serverPacket.createPacket(serverClient));
			}

		}
	}

	/*
	 * Package structure
	 * 
	 * | 8 byte | 120 byte |
	 * 
	 * info data
	 */

	public class ServerThread extends Thread {

		private byte[] buffer = new byte[ServerPacket.PACKAGE_SIZE];
		private DatagramPacket packet = new DatagramPacket(buffer,
				buffer.length);
		private ServerPacket serverPacket = new ServerPacket();
		private Server server;

		public ServerThread(Server server) {
			this.server = server;
		}

		@Override
		public void run() {

			Log.i(Game.TAG, "Server: started");

			try {
				while (!Thread.currentThread().isInterrupted()) {
					packet = new DatagramPacket(buffer, buffer.length);
					serverSocket.receive(packet);
					if (!ServerPacket.isValidArray(buffer)) {
						continue;
					}

					serverPacket.set(packet);

					switch (serverPacket.getType()) {
					case REGISTER: {

						if (registrate) {

							if (!serverClients
									.containsKey(serverPacket.address)) {
								String name = new String(
										serverPacket.getData(), 0,
										serverPacket.getData().length);
								serverClients.put(
										serverPacket.getAddress(),
										new ServerClient(serverPacket
												.getAddress(), serverPacket
												.getPort(), name));

								eventSource.fireEventClientAccepted(server,
										name, 0);

								Log.i(Game.TAG, "Server: client '" + name
										+ "' connected");

							}

							ServerPacket pack = new ServerPacket(Type.REGISTER,
									serverName.getBytes());
							pack.setAddress(serverPacket.getAddress());
							pack.setPort(serverPacket.getPort());

							server.send(pack);
						}
						break;
					}
					case UNREGISTER: {
						if (serverClients.containsKey(serverPacket.address)) {
							String name = serverClients
									.get(packet.getAddress()).name;
							serverClients.remove(packet.getAddress());

							eventSource.fireEventClientDisconnected(server,
									name, 0);
						}
						break;
					}
					case DATA: {
						if (serverClients.containsKey(serverPacket.address)) {
							gameThread.push(new GameMessage(serverPacket));
						}
						break;
					}
					default: {
						break;
					}
					}

					Thread.sleep(1);

				}
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} catch (IOException e) {
				Log.e(Game.TAG, "Server: server thread IOException");
				e.printStackTrace();
			}
			
			if (serverSocket != null && !serverSocket.isClosed()) {
				serverSocket.close();
			}
			
			Log.i(Game.TAG, "Server: thread finished");
		}
	}

	public class ServerClient {

		protected InetAddress address;
		protected int port;
		protected String name;

		public ServerClient(InetAddress address, int port, String name) {
			this.address = address;
			this.port = port;
			this.name = name;
		}
	}
}

package com.swannteam.carbreaker.network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import android.os.Debug;
import android.util.Log;

import com.bulletphysics.collision.narrowphase.ManifoldPoint;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.InternalTickCallback;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.vehicle.DefaultVehicleRaycaster;
import com.bulletphysics.dynamics.vehicle.VehicleTuning;
import com.bulletphysics.linearmath.Transform;
import com.swannteam.carbreaker.game.Game;
import com.swannteam.carbreaker.game.GameUtils;
import com.swannteam.carbreaker.game.IGameConfig;
import com.swannteam.carbreaker.network.Server.ServerClient;
import com.swannteam.carbreaker.network.events.ServerEventSource;
import com.swannteam.carbreaker.world.Car;
import com.swannteam.carbreaker.world.IVehicle;
import com.swannteam.carbreaker.world.RealBody;
import com.swannteam.carbreaker.world.RealWorld;

public class GameServerThread extends Thread {

	private static final int MAX_MESSAGES_PER_TICK = 60;
	private static int SEND_MESSAGE_EVERY_TICK = 5;

	private static final float HEALTH_COEFFICIENT = 0.5f;
	private static final float DAMAGE_COEFFICIENT = 2;

	protected HashMap<Integer, Byte> players = new HashMap<Integer, Byte>();
	private ConcurrentLinkedQueue<GameMessage> queue = new ConcurrentLinkedQueue<GameMessage>();

	private RealWorld realWorld;

	private Server server;
	private ServerEventSource eventSource;
	private GameMessageHelper msgHelper = new GameMessageHelper();

	private ArrayList<IVehicle> cars;
	private ArrayList<Boolean> playersReady = new ArrayList<Boolean>();

	private GameMessage msg;

	private long counter = 0;
	private boolean initialized = false;

	public GameServerThread(Server server, ServerEventSource eventSource) {
		this.server = server;
		this.eventSource = eventSource;
	}

	public void push(GameMessage message) {
		switch (message.getCommandType()) {
		case READY: {
			if (initialized) {
				byte id = players.get(message.addressHash);
				playersReady.set(id, true);
				eventSource.fireEventClientReady(server, "", id);
			}

			break;
		}
		default: {
			queue.add(message);
			break;
		}
		}
	}

	public boolean isEveryoneReady() {
		if (!initialized) {
			return false;
		}

		for (Boolean bool : playersReady) {
			if (!bool) {
				return false;
			}
		}

		return true;
	}

	public boolean initialize(IGameConfig gameConfig) {
		if (!gameConfig.initialize(Game.getInstance().getActivity())) {
			return false;
		}

		ArrayList<ServerClient> clients = new ArrayList<ServerClient>(
				server.serverClients.values());
		int clientsCount = clients.size();

		for (byte i = 0; i < clients.size(); ++i) {
			players.put(clients.get(i).address.hashCode(), i);
		}

		// RealWorld

		realWorld = new RealWorld();
		realWorld.resetDynamicWorld();

		// World

		float mass = 0;
		Transform startTransform = new Transform();
		startTransform.setIdentity();
		startTransform.origin.set(0, 0, 0);

		realWorld.add(new RealBody(null, GameUtils.CreateRigidBodyFromShape(
				gameConfig.getGroundCollisionShape(), mass, new Vector3f(0, 0,
						0), startTransform)));

		// Cars

		Vector3f[] connectionPoints = gameConfig.getConnectionPoints();
		float wheelRadius = gameConfig.getWheelRadius();
		float suspensionRestLength = gameConfig.getSuspensionRestLength();

		for (int i = 0; i < clientsCount; ++i) {
			RigidBody carBody = GameUtils.CreateRigidBodyFromShape(
					gameConfig.getCarCollisionShape(), gameConfig.getCarMass(),
					new Vector3f(0, 0, 0), gameConfig.getStartTransforms()[i]);

			Car car = new Car(null, carBody, new VehicleTuning(),
					new DefaultVehicleRaycaster(realWorld.getDynamicsWorld()));
			car.setHealth(gameConfig.getCarHealth());
			car.getRigidBody().setCenterOfMassTransform(
					gameConfig.getCentersOfMass()[i]);

			realWorld.addVehicle(car);
			playersReady.add(false);

			car.setupDefaultWheels(realWorld, null, connectionPoints,
					suspensionRestLength, wheelRadius);
		}

		realWorld.getDynamicsWorld().setInternalTickCallback(
				new InternalTickCallback() {

					RigidBody body1;
					RigidBody body2;

					private Car car1;
					private Car car2;

					private Vector3f carSpeed1 = new Vector3f();
					private Vector3f carSpeed2 = new Vector3f();

					private Vector3f delta = new Vector3f();

					@Override
					public void internalTick(DynamicsWorld arg0, float arg1) {

						int numManifolds = realWorld.getDynamicsWorld()
								.getDispatcher().getNumManifolds();

						for (int i = 0; i < numManifolds; ++i) {
							PersistentManifold contactManifold = realWorld
									.getDynamicsWorld().getDispatcher()
									.getManifoldByIndexInternal(i);
							int numContacts = contactManifold.getNumContacts();
							for (int j = 0; j < numContacts; ++j) {
								ManifoldPoint pt = contactManifold.getContactPoint(j);
								if (pt.getDistance() < 0.f) {
									if (contactManifold.getBody0() instanceof RigidBody
											&& contactManifold.getBody1() instanceof RigidBody) {

										body1 = (RigidBody) contactManifold
												.getBody0();
										body2 = (RigidBody) contactManifold
												.getBody1();

										car1 = (Car) body1.getUserPointer();
										car2 = (Car) body2.getUserPointer();

										if (car1 == null || car2 == null) {
											break;
										}

										body1.getLinearVelocity(carSpeed1);
										body2.getLinearVelocity(carSpeed2);

										delta = new Vector3f(carSpeed1);
										delta.sub(carSpeed2);

										if (carSpeed1.length() < carSpeed2
												.length()) {
											car1.setHealth(car1.getHealth()
													- delta.length()
													* HEALTH_COEFFICIENT
													* DAMAGE_COEFFICIENT);
											car2.setHealth(car2.getHealth()
													- delta.length()
													* HEALTH_COEFFICIENT);
										} else {
											car1.setHealth(car1.getHealth()
													- delta.length()
													* HEALTH_COEFFICIENT);
											car2.setHealth(car2.getHealth()
													- delta.length()
													* HEALTH_COEFFICIENT);
										}
									}
								}
							}
						}
					}
				}, null);

		Log.i(Game.TAG, "Server game: initialized");

		initialized = true;
		return true;
	}

	private float[] carDataBuf = new float[3];

	@Override
	public void run() {

		if (!initialized) {
			return;
		}

		Log.i(Game.TAG, "Server: game thread started");

		cars = realWorld.getVehicles();
		Collection<Byte> ids = players.values();

		try {
			while ((!Thread.currentThread().isInterrupted())) {

				if (realWorld == null) {
					break;
				}

				synchronized (realWorld) {

					for (int i = 0; i < MAX_MESSAGES_PER_TICK; ++i) {
						msg = queue.poll();
						if (msg == null) {
							break;
						}

						byte id = players.get(msg.addressHash);
						Car car = (Car) cars.get(id);

						if (car.isAlive()) {

							switch (msg.getCommandType()) {
							case CAR_DATA: {
								float[] data = msg.getFloatArray();

								car.setData(data[0], data[1], data[2]);
								break;
							}
							case RESET_POSITION: {

								resetPosition(id);
								break;
							}
							default:
								break;
							}

						}

					}

					realWorld.tick();

					msg = new GameMessage();

					if (counter % SEND_MESSAGE_EVERY_TICK == 0) {

						counter -= SEND_MESSAGE_EVERY_TICK;

						for (byte id : ids) {
							broadcast(id);
						}
					}

					++counter;
				}

				Thread.sleep(1);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		msg = new GameMessage();
		msgHelper.setGameData(msg, CommandType.EXIT);

		server.sendToAll(msg.getServerPacket());

		Log.i(Game.TAG, "Server: game thread finished");
	}

	public void resetPosition(byte id) {
		Car car = (Car) cars.get(id);

		car.getRigidBody().setLinearVelocity(new Vector3f(0, 0, 0));
		car.getRigidBody().setAngularVelocity(new Vector3f(0, 0, 0));

		Quat4f q = new Quat4f();
		car.getTransform().getRotation(q);
		q.x = 0;
		// q.y = 0;
		q.z = 0;

		car.setData(0, 0, 0);
		Transform tr = car.getTransform();
		tr.basis.setIdentity();
		if (tr.origin.y < -10) {
			tr.origin.x = 0;
			tr.origin.y = 2;
			tr.origin.z = 0;
		}

		tr.origin.y = tr.origin.y + 1;

		tr.setRotation(q);
		car.setTransform(tr);
	}

	public void broadcast(byte id) {
		Car car = (Car) cars.get(id);
		car.getData(carDataBuf);

		msgHelper.setTransform(msg, car.getTransform());
		msgHelper.setGameData(msg, CommandType.TRANSFORM, id);

		server.sendToAll(msg.getServerPacket());

		msgHelper.setCarData(msg, carDataBuf[0], carDataBuf[1], carDataBuf[2]);
		msgHelper.setGameData(msg, CommandType.CAR_DATA, id);

		server.sendToAll(msg.getServerPacket());

		msgHelper.setCarHealth(msg, car.getHealth());
		msgHelper.setGameData(msg, CommandType.CAR_HEALTH, id);

		server.sendToAll(msg.getServerPacket());
	}

	public int hasWinner() {
		int count = 0;
		int win = 0;
		for (int i = 0; i < cars.size(); i++) {
			if (count > 0) {
				return -1;
			}
			if (((Car) cars.get(i)).isAlive()) {
				++count;
				win = i;
			}
		}
		return win;
	}
}

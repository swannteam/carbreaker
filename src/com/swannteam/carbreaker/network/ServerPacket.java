package com.swannteam.carbreaker.network;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Arrays;

import com.swannteam.carbreaker.network.Server.ServerClient;

public class ServerPacket {


	public static final int PACKAGE_SIZE = 60;
	public static final int DATA_SECTION = 8;

	public enum Type {
		UNREGISTER, REGISTER, DATA, NEW_USER
	}

	protected byte[] info;
	protected byte[] data;

	protected InetAddress address;
	protected int port;

	public ServerPacket() {
	}

	public ServerPacket(byte[] info, byte[] data) {
		this.info = info;
		this.data = data;
	}

	public ServerPacket(byte[] info, byte[] data, InetAddress address, int port) {
		this.info = info;
		this.data = data;

		this.address = address;
		this.port = port;
	}

	public ServerPacket(byte[] bytes) {
		this.info = Arrays.copyOfRange(bytes, 0, DATA_SECTION);
		this.data = Arrays.copyOfRange(bytes, DATA_SECTION, bytes.length);
	}

	public ServerPacket(Type type, byte[] data) {
		byte[] info = new byte[DATA_SECTION];
		info[0] = (byte) type.ordinal();
		this.info = info;
		this.data = data;
	}

	public ServerPacket(DatagramPacket packet) {
		byte[] bytes = packet.getData();

		info = Arrays.copyOfRange(bytes, 0, DATA_SECTION);
		data = Arrays.copyOfRange(bytes, DATA_SECTION, packet.getLength());

		address = packet.getAddress();
		port = packet.getPort();
	}

	public void set(DatagramPacket packet) {
		byte[] bytes = packet.getData();
		
		info = Arrays.copyOfRange(bytes, 0, DATA_SECTION);
		data = Arrays.copyOfRange(bytes, DATA_SECTION, packet.getLength());

		address = packet.getAddress();
		port = packet.getPort();
	}
	
	public byte[] getInfo() {
		return info;
	}

	public byte[] getData() {
		return data;
	}

	public InetAddress getAddress() {
		return address;
	}

	public void setAddress(InetAddress address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public void setPacket(DatagramPacket packet){
		byte[] bytes = packet.getData();

		info = Arrays.copyOfRange(bytes, 0, DATA_SECTION);
		data = Arrays.copyOfRange(bytes, DATA_SECTION, packet.getLength());

		address = packet.getAddress();
		port = packet.getPort();
	}

	public byte[] getBytes() {
		if (data != null) {
			byte[] bytes = new byte[info.length + data.length];
			System.arraycopy(data, 0, bytes, info.length, data.length);
			System.arraycopy(info, 0, bytes, 0, info.length);

			return bytes;
		} else {
			byte[] bytes = new byte[info.length];
			System.arraycopy(info, 0, bytes, 0, info.length);

			return bytes;
		}
	}

	public Type getType() {
		return Type.values()[info[0]];
	}

	public static boolean isValidArray(byte[] bytes) {

		if (bytes.length < DATA_SECTION) {
			return false;
		}
		return true;
	}

	private byte[] byteBuf;

	public DatagramPacket createPacket() {
		byteBuf = getBytes();
		return new DatagramPacket(byteBuf, byteBuf.length, address, port);
	}

	public DatagramPacket createPacket(ServerClient serverClient) {
		byteBuf = getBytes();
		return new DatagramPacket(byteBuf, byteBuf.length,
				serverClient.address, serverClient.port);
	}
}

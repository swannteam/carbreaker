package com.swannteam.carbreaker.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.swannteam.carbreaker.game.Game;

import android.util.Log;

public class Sender extends Thread {

	private static final int MAX_MESSAGES = 1000;

	private DatagramSocket soket;
	private ConcurrentLinkedQueue<DatagramPacket> packetQueue = new ConcurrentLinkedQueue<DatagramPacket>();
	private DatagramPacket packet;

	public Sender(DatagramSocket host) {
		soket = host;
	}

	public Sender(int port) throws SocketException {
		soket = new DatagramSocket(port);
	}

	long time1;
	long time2;

	public void run() {
		try {
			while ((!Thread.currentThread().isInterrupted())) {
				for (int i = 0; i < MAX_MESSAGES; ++i) {
					
					packet = packetQueue.poll();
					if (packet == null) {
						break;
					}

					soket.send(packet);
				}

				Thread.sleep(1);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void push(DatagramPacket packet) {
		packetQueue.add(packet);
	}

}

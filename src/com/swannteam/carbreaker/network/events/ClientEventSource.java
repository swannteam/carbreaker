package com.swannteam.carbreaker.network.events;

import java.util.ArrayList;

import com.swannteam.carbreaker.network.Client;

public class ClientEventSource {

	private ArrayList<ClientListener> listeners = new ArrayList<ClientListener>();
	
	public ClientEventSource() {
		// TODO Auto-generated constructor stub
	}

	public synchronized void addEventListener(ClientListener listener) {
		listeners.add(listener);
	}

	public synchronized void removeEventListener(ClientListener listener) {
		listeners.remove(listener);
	}

	public synchronized void fireEventClientAccepted(Client client, String message, int data) {
		ClientEvent event = new ClientEvent(this, client, message, data);
		for (ClientListener listener : listeners) {
			listener.onClientAccepted(event);
		}
	}

	public synchronized void fireEventClientReady(Client client, String message, int data) {
		ClientEvent event = new ClientEvent(this, client, message, data);
		for (ClientListener listener : listeners) {
			listener.onClientReady(event);
		}
	}

	public synchronized void fireEventClientDisconnected(Client client, String message, int data) {
		ClientEvent event = new ClientEvent(this, client, message, data);
		for (ClientListener listener : listeners) {
			listener.onClientDisconnected(event);
		}
	}

	public synchronized void fireEventGameInitialized(Client client, String message, int data) {
		ClientEvent event = new ClientEvent(this, client, message, data);
		for (ClientListener listener : listeners) {
			listener.onGameInitialized(event);
		}
	}

	public synchronized void fireEventGameStarted(Client client, String message, int data) {
		ClientEvent event = new ClientEvent(this, client, message, data);
		for (ClientListener listener : listeners) {
			listener.onGameStarted(event);
		}
	}

	public synchronized void fireEventGameFinished(Client client, String message, int data) {
		ClientEvent event = new ClientEvent(this, client, message, data);
		for (ClientListener listener : listeners) {
			listener.onGameFinished(event);
		}
	}	
}

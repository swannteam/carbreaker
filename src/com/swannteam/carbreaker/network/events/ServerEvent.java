package com.swannteam.carbreaker.network.events;

import java.util.EventObject;

import com.swannteam.carbreaker.network.Server;

public class ServerEvent extends EventObject {

	private static final long serialVersionUID = 2410909048962394676L;
	
	private Server server;
	private String message;
	private int data;

	public ServerEvent(Object source) {
		super(source);
	}
	
	public ServerEvent(Object source, Server server, String message, int data) {
		super(source);
		this.server = server;
		this.message = message;
		this.data = data;
	}
	
	public Server getServer() {
		return server;
	}

	public String getMessage() {
		return message;
	}

	public int getData() {
		return data;
	}
}

package com.swannteam.carbreaker.network.events;

import java.util.EventListener;

public interface ServerListener extends EventListener {
	
	void onClientAccepted(ServerEvent e);
	void onClientReady(ServerEvent e);
	void onClientDisconnected(ServerEvent e);
	
	void onGameInitialized(ServerEvent e);
	void onGameStarted(ServerEvent e);
	void onGameFinished(ServerEvent e);
}

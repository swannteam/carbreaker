package com.swannteam.carbreaker.network.events;

import java.util.ArrayList;

import com.swannteam.carbreaker.network.Server;

public class ServerEventSource {

	private ArrayList<ServerListener> listeners = new ArrayList<ServerListener>();
	
	public ServerEventSource() {
	}

	public synchronized void addEventListener(ServerListener listener) {
		listeners.add(listener);
	}

	public synchronized void removeEventListener(ServerListener listener) {
		listeners.remove(listener);
	}

	public synchronized void fireEventClientAccepted(Server server, String message, int data) {
		ServerEvent event = new ServerEvent(this, server, message, data);
		for (ServerListener listener : listeners) {
			listener.onClientAccepted(event);
		}
	}

	public synchronized void fireEventClientReady(Server server, String message, int data) {
		ServerEvent event = new ServerEvent(this, server, message, data);
		for (ServerListener listener : listeners) {
			listener.onClientReady(event);
		}
	}

	public synchronized void fireEventClientDisconnected(Server server, String message, int data) {
		ServerEvent event = new ServerEvent(this, server, message, data);
		for (ServerListener listener : listeners) {
			listener.onClientDisconnected(event);
		}
	}

	public synchronized void fireEventGameInitialized(Server server, String message, int data) {
		ServerEvent event = new ServerEvent(this, server, message, data);
		for (ServerListener listener : listeners) {
			listener.onGameInitialized(event);
		}
	}

	public synchronized void fireEventGameStarted(Server server, String message, int data) {
		ServerEvent event = new ServerEvent(this, server, message, data);
		for (ServerListener listener : listeners) {
			listener.onGameStarted(event);
		}
	}

	public synchronized void fireEventGameFinished(Server server, String message, int data) {
		ServerEvent event = new ServerEvent(this, server, message, data);
		for (ServerListener listener : listeners) {
			listener.onGameFinished(event);
		}
	}

}

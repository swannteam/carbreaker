package com.swannteam.carbreaker.network.events;

public class ServerAdapter implements ServerListener {

	@Override
	public void onClientAccepted(ServerEvent e) {		
	}

	@Override
	public void onClientReady(ServerEvent e) {		
	}

	@Override
	public void onClientDisconnected(ServerEvent e) {		
	}

	@Override
	public void onGameInitialized(ServerEvent e) {		
	}

	@Override
	public void onGameStarted(ServerEvent e) {		
	}

	@Override
	public void onGameFinished(ServerEvent e) {		
	}

}

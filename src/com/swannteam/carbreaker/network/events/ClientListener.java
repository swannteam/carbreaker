package com.swannteam.carbreaker.network.events;

import java.util.EventListener;

public interface ClientListener extends EventListener {
	
	void onClientAccepted(ClientEvent e);
	void onClientReady(ClientEvent e);
	void onClientDisconnected(ClientEvent e);
	
	void onGameInitialized(ClientEvent e);
	void onGameStarted(ClientEvent e);
	void onGameFinished(ClientEvent e);
}

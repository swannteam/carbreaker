package com.swannteam.carbreaker.network.events;

public class ClientAdapter implements ClientListener {

	public ClientAdapter() {
	}

	@Override
	public void onClientAccepted(ClientEvent e) {
	}

	@Override
	public void onClientReady(ClientEvent e) {
	}

	@Override
	public void onClientDisconnected(ClientEvent e) {
	}

	@Override
	public void onGameInitialized(ClientEvent e) {
	}

	@Override
	public void onGameStarted(ClientEvent e) {
	}

	@Override
	public void onGameFinished(ClientEvent e) {
	}

}

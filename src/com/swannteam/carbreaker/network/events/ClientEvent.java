package com.swannteam.carbreaker.network.events;

import java.util.EventObject;

import com.swannteam.carbreaker.network.Client;

public class ClientEvent extends EventObject {

	private static final long serialVersionUID = 5432272891177054554L;
	
	private Client client;
	private String message;
	private int data;
	
	public ClientEvent(Object source) {
		super(source);
	}
	
	public ClientEvent(Object source, Client client, String message, int data) {
		super(source);
		this.client = client;
		this.message = message;
		this.data = data;
	}

	public Client getClient() {
		return client;
	}

	public String getMessage() {
		return message;
	}

	public int getData() {
		return data;
	}
}

package com.swannteam.carbreaker.network;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class GameMessage {

	protected byte[] info;
	protected byte[] data;
	
	protected int addressHash;
	
	public GameMessage() {
	}
	
	public GameMessage(byte[] info, byte[] data) {
		this.info = info;
		this.data = data;
	}
	
	public GameMessage(ServerPacket packet) {
		this.info = packet.info;
		this.data = packet.data;
		addressHash = packet.address.hashCode();
	}
	
	public byte[] getInfo() {
		return info;
	}
	
	public byte[] getData() {
		return data;
	} 
	
	public void setInfo(byte[] info) {
		this.info = info;
	}
	
	public void setData(byte[] data) {
		this.data = data;
	}
	
	public void setData(float[] floatData) {
		data = new byte[floatData.length * 4];

		ByteBuffer byteBuffer = ByteBuffer.wrap(data);
		FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
		floatBuffer.put(floatData);
	}
	
	public int getAddressHash() {
		return addressHash;
	}
	
	public void setAddressHash(int addressHash) {
		this.addressHash = addressHash;
	}
	
	public CommandType getCommandType() {
		return CommandType.values()[info[1]];
	}
	
	public String getString() {
		return new String(data);
	}
	
	public int getId() {
		return info[2] << 24 | (info[3] & 0xFF) << 16 | (info[4] & 0xFF) << 8 | (info[5] & 0xFF);
	}
	
	public float[] getFloatArray() {
		float floatArray[] = new float[data.length / 4];

		ByteBuffer byteBuf = ByteBuffer.wrap(data);
		FloatBuffer floatBuf = byteBuf.asFloatBuffer();
		floatBuf.get(floatArray);

		return floatArray;
	}
	
	public ServerPacket getServerPacket() {
		return new ServerPacket(info, data);
	}
}

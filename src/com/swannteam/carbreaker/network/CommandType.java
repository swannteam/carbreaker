package com.swannteam.carbreaker.network;

public enum CommandType {

	CAR_DATA,
	TRANSFORM,
	CAR_HEALTH,

	GO,
	INIT,
	READY,
	
	RESET_POSITION,

	EXIT;
}

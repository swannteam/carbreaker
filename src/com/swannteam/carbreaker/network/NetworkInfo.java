package com.swannteam.carbreaker.network;

public class NetworkInfo {

    public static final String DEFAULT_CLIENT_NAME = "CarBreaker Client";
    public static final String DEFAULT_SERVER_NAME = "CarBreaker Server";
    
    public static final int SERVER_PORT = 9595;
    public static final int CLIENT_PORT = 9594;
    
    public static final int DEFAULT_MAX_CLIENT = 10;

}

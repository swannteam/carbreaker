package com.swannteam.carbreaker.graphics;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.swannteam.carbreaker.game.Game;
import com.threed.jpct.FrameBuffer;

import android.opengl.GLSurfaceView;

public class GameRenderer implements GLSurfaceView.Renderer {

	@Override
	public void onDrawFrame(GL10 arg0) {
		Game.getInstance().recalc();
	}

	@Override
	public void onSurfaceChanged(GL10 arg0, int arg1, int arg2) {
		Game.getInstance().init(new FrameBuffer(arg1, arg2));
	}

	@Override
	public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
		//Game.getInstance().preInit();
	}

}
